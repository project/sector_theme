<?php

/**
 * @file
 * Theme and preprocess functions for menus.
 */

/**
 * Implements hook_preprocess_menu().
 */
function sector_theme_preprocess_menu(&$variables, $hook) {
  // No changes for menu toolbar.
  if ($hook == 'menu__toolbar') {
    return;
  }

  // Get the current path.
  $current_path = \Drupal::request()->getRequestUri();

  foreach ($variables['items'] as $k => &$item) {
    sector_theme_recursive_add_menu_link_attributes($item, $current_path);
  }
}

/**
 * Implements template_preprocess_menu_local_action().
 */
function sector_theme_preprocess_menu_local_action(&$variables) {
  // Add button classes.
  $variables['link']['#options']['attributes']['class'][] = 'btn';
  $variables['link']['#options']['attributes']['class'][] = 'btn-primary';
}

/**
 * Implements template_preprocess_links__dropbutton().
 */
function sector_theme_preprocess_links__dropbutton(&$variables) {
  $links = &$variables['links'];

  // Do nothing if we have no links.
  if (!count($links)) {
    return;
  }

  // Get the first link and use it for the dropbutton.
  $link = reset($links);

  $variables['split'] = FALSE;
  if (isset($link['link']) && ($url = $link['link']['#url'])) {
    $button = $link['link'];

    if ($variables['split'] = $url->getRouteName() !== '<nolink>') {
      $button['#options']['attributes']['class'][] = 'btn';
      $button['#options']['attributes']['class'][] = 'btn-outline-dark';
    }

    $variables['button'] = $button;

    // Remove first link from links.
    array_shift($links);

    foreach ($links as $key => $link) {
      $links[$key]['link']['#options']['attributes']['class'][] = 'dropdown-item';
    }

  }
  elseif (isset($link['text'])) {
    $button = $link['text'];
    $variables['button'] = $button;

    // Remove first link from links.
    array_shift($links);
  }

  // Add required classes.
  foreach ($links as $key => $link) {
    if (isset($links[$key]['text_attributes'])) {
      $links[$key]['text_attributes']->addClass('dropdown-item');
    }

    if (isset($links[$key]['attributes'])) {
      $links[$key]['attributes']->addClass('dropdown-item');
    }
  }
}

/**
 * Loop through menu item children, make sure menu_link_attributes attributes are available.
 */
function sector_theme_recursive_add_menu_link_attributes(&$item, $current_path) {
  // Add your custom property to the menu item.
  $menu_link = $item['original_link'] ?? NULL;
  $options = !empty($menu_link) ? $menu_link->getOptions() : NULL;
  $item['extra'] = $options['attributes'] ?? NULL;
  $item['current'] = FALSE;
  $item['published'] = TRUE;

  if (!$item['url']->isExternal() && $item['url']->isRouted()) {

    // RouteParameters gets the NID of the linked item.
    $routeParameters = $item['url']->getRouteParameters();
    if ($routeParameters) {
      $nid = (!empty($routeParameters['node'])) ? $routeParameters['node'] : FALSE;
      if (!empty($nid)) {
        $query = \Drupal::entityQuery('node');
        $query->accessCheck(TRUE);
        $query->condition('status', 0);
        $query->condition('nid', $nid);
        $entity = $query->execute();
        if ($entity) {
          $item['published'] = FALSE;
        }
      }
    }
  }

  if ($item['url']->toString() === $current_path) {
    $item['current'] = TRUE;
  }

  // Check if the menu item has children and process them recursively.
  if (!empty($item['below'])) {
    foreach ($item['below'] as &$child_item) {
      sector_theme_recursive_add_menu_link_attributes($child_item, $current_path);
    }
  }
}