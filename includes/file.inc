<?php

use JCoded\FriendlyMime\FriendlyMime;

/**
 * Implements hook_preprocess_file_link().
 */
function sector_theme_preprocess_file_link(&$variables) {
    $file = $variables['file'];

    $query = \Drupal::entityQuery('media')
        ->accessCheck(TRUE)
        ->condition('bundle', 'document')
        ->condition('field_media_document.target_id', $file->id());
    $query->accessCheck(TRUE);
    $result = $query->execute();
    if (!empty($result)) {
        $media = \Drupal::entityTypeManager()->getStorage('media')->load(reset($result));
        $variables['link']['#title'] = $media->label();
    }

    $variables['file_type'] = [
        'human' => FriendlyMime::getFriendlyName($file->getMimeType()),
        'extension' => pathinfo($file->getFileUri(), PATHINFO_EXTENSION),
    ];
}