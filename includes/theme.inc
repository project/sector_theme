<?php

/**
 * @file
 * Custom theme hooks.
 */

/**
 * Preprocess variables for all templates.
 * @param $variables
 */
function sector_theme_preprocess(&$variables) {
  // Inject the current language into every template
  $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
  $variables['language'] ??= $language;

  global $base_url;
  $variables['base_url'] ??= $base_url;

  $variables['path'] ??= \Drupal::service('path.current')->getPath();
}

function sector_theme_theme_suggestions_container_alter(array &$suggestions, array $variables, $hook) {
  $element = $variables['element'];

  if (isset($element['#type'])) {
    $suggestions[] = $hook . '__' . preg_replace('/[^a-zA-Z0-9_]+/', '_', $element['#type']);
  }
}