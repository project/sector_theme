Drupal.behaviors.prose = {
  attach: (context) => {
    context.querySelectorAll(".prose table").forEach((table) => {
      const wrapperDiv = document.createElement("div");
      wrapperDiv.className = "table-wrap";
      table.parentNode.insertBefore(wrapperDiv, table);
      wrapperDiv.appendChild(table);
    });
  },
};
