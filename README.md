# Sector Base Theme

This is the Sector Distribution base theme, heavily influenced by the great [Radix theme](https://www.drupal.org/project/radix). It bundles [Tailwind](https://tailwindcss.com/), [esbuild](https://esbuild.github.io/), [BrowserSync](https://browsersync.io/) and [Google Material Symbols](https://fonts.google.com/icons).

## Requirements
1. pnpm - [read how to install here](https://pnpm.io/installation).
2. Drush - [read how to install here](https://www.drush.org/en/master/install/).
3. [Single Directory Components](https://www.drupal.org/project/sdc)

## Creating a subtheme
- Create the directory where you'd like to keep your custom subtheme. We recommend `web/themes/custom`.
- Generate subtheme from the starter kit (from inside `web`): `php core/scripts/drupal generate-theme --starterkit sector_starterkit my_new_theme --path themes/custom`
- Set as default theme: `drush en my_new_theme -y; drush config-set system.theme default my_new_theme -y`
- Install required packages: `cd /web/themes/custom/my_new_theme; pnpm install`
- Build: `pnpm build`

### ‼️ Notes
If you are setting up a new subtheme on a fresh Sector Distribution instance, make sure you disable Sector Demo after creating your custom subtheme (described above).

If you are contributing to the theme (testing the compiled theme), and need to pnpm install and pnpm build 'Sector Demo' you'll need to add `- web/themes/contrib/sector_theme/themes/sector_demo` to /pnpm-workspace.yaml > packages

### Development
- `pnpm dev`

  * will run `watch` and `browsersync` in parallel

#### Use BrowserSync

- Update the `sector10.ddev.site` value in `package.json` with your local site url.

