<?php

/**
 * @file
 * Theme and preprocess functions for nodes.
 */

use Drupal\Core\Template\Attribute;

function sector_starterkit_preprocess_node(&$variables) {
  if ($variables['page'] === FALSE) {
    $variables['title_link_attributes'] = new Attribute();
  }
}
