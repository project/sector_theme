/** @type {import('tailwindcss').Config} */

import { DefaultColors } from 'tailwindcss/types/generated/colors';

const colors = require('tailwindcss/colors')

module.exports = {
  mode: 'jit',
  darkMode: 'class',
  content: [
    './templates/**/*.{html,twig}',
    './src/**/*.{html,twig}',
    './components/**/*.{html,twig}',
    '../../src/layout/*/*.{html,twig}',
    '../../templates/**/*.{html,twig}',
    '../../src/components/**/*.{html,twig}',
    '../../components/**/*.{html,twig}',
    './node_modules/safelist.txt',
  ],
  theme: {
    container: {
      center: true,
      padding: 'var(--container-padding, 2rem)',
    },
    extend: {
      breakpoints: {
        '2xl': '1536px',
        '3xl': '1920px',
        '4xl': '2560px',
      },
      colors: {
        alert: `var(--alert, ${colors.orange["800"]})`,
        notice: `var(--alert, ${colors.amber["700"]})`,
        highlight: `var(--alert, ${colors.amber["100"]})`,
        brand: {
          DEFAULT: 'oklch(91.91% 0.22 102.16)',
          primary: {
            light: `var(--brand-primary-light, ${colors.cyan['500']})`,
            medium: `var(--brand-primary-medium, ${colors.cyan['700']})`,
            dark: `var(--brand-primary-dark, ${colors.cyan['900']})`,
          },
          secondary: {
            light: `var(--brand-secondary-light, ${colors.amber['500']})`,
            medium: `var(--brand-secondary-medium, ${colors.amber['700']})`,
            dark: `var(--brand-secondary-dark, ${colors.amber['900']})`
          },
          neutral: {
            light: `var(--brand-neutral-light, ${colors.neutral['200']})`,
            medium: `var(--brand-neutral-medium, ${colors.neutral['400']})`,
            dark: `var(--brand-neutral-dark, ${colors.neutral['600']})`
          }
        },
        type: {
          dark: {
            DEFAULT: `var(--type-dark-primary, ${colors.neutral['900']})`,
            primary: `var(--type-dark-primary, ${colors.neutral['900']})`,
            secondary: `var(--type-dark-secondary, ${colors.neutral['700']})`,
            tertiary: `var(--type-dark-tertiary, ${colors.neutral['500']})`,
          },
          light: {
            DEFAULT: `var(--type-light-primary, ${colors.white})`,
            primary: `var(--type-light-primary, ${colors.white})`,
            secondary: `var(--type-light-secondary, ${colors.neutral['50']})`,
            tertiary: `var(--type-light-tertiary, ${colors.neutral['200']})`,
          }
        },
        surface: {
          dark: {
            DEFAULT: `var(--surface-dark-primary, var(--type-dark-primary))`,
            primary: `var(--surface-dark-primary, var(--type-dark-primary))`,
            secondary: `var(--surface-dark-secondary, var(--type-dark-secondary))`,
            tertiary: `var(--surface-dark-tertiary, var(--type-dark-tertiary))`,
          },
          light: {
            DEFAULT: `var(--surface-light-primary, var(--type-light-primary))`,
            primary: `var(--surface-light-primary, var(--type-light-primary))`,
            secondary: `var(--surface-light-secondary, var(--type-light-secondary))`,
            tertiary: `var(--surface-light-tertiary, var(--type-light-tertiary))`,
          }
        },
        interaction: {
          primary: {
            DEFAULT: `var(--interaction-primary-base, ${colors.cyan['700']})`,
            base: `var(--interaction-primary-base, ${colors.cyan['700']})`,
            dark: `var(--interaction-primary-dark, ${colors.cyan['900']})`,
          },
          light: {
            DEFAULT: `var(--interaction-light-base, ${colors.white})`,
            base: `var(--interaction-light-base, ${colors.white})`,
          },
          dark: {
            DEFAULT: `var(--interaction-dark-base, ${colors.neutral['900']})`,
            base: `var(--interaction-dark-base, ${colors.neutral['900']})`,
          },
          link: {
            DEFAULT: `var(--interaction-link-base, ${colors.cyan['500']})`,
            visited: `var(--interaction-link-visited, ${colors.purple['500']})`,
            hover: `var(--interaction-link-hover, ${colors.cyan['600']})`,
            active: `var(--interaction-link-active, ${colors.cyan['700']})`,
          }
        },
        status: {
          DEFAULT: `var(--status-note, ${colors.cyan['700']})`,
          note: `var(--status-note, ${colors.cyan['700']})`,
          warning: `var(--status-warning, ${colors.yellow['500']})`,
          success: `var(--status-success, ${colors.lime['600']})`,
          error: `var(--status-error, ${colors.orange['700']})`,
        },
        notice: {
          DEFAULT: colors.cyan['900'],
          base: colors.cyan['900'],
          moderate: colors.cyan['700'],
          critical: colors.amber['600'],
          'highly-critical': colors.amber['900'],
        }
      },
      fontFamily: {
        custom: ['var(--custom-font)'],
        'custom-alt': ['var(--custom-font-alternate)'],
        'custom-display': ['var(--custom-display)'],
      },
      fontSize: {
        'flexi-1': 'clamp(24px, 10vmax, 52px)',
      },
      keyframes: {
        fadeInUp: {
          from: { transform: 'translateY(1em)', opacity: 0 },
          to: { transform: 'translateY(0)', opacity: 1 },
        },
      },
      animation: {
        alert: 'fadeInUp .35s ease',
      },
    },
  },
  plugins: [
    require('@tailwindcss/container-queries'),
    require('@tailwindcss/forms')({
      strategy: 'base',
    }),
    require('@tailwindcss/typography')({
      className: 'wysiwyg',
    }),
    require('tailwind-safelist-generator')({
      path: 'node_modules/safelist.txt',
      patterns: require('./build/tailwind-safelist'),
    }),
  ],
};
