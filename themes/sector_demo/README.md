# Installation

We're using PNPM `npm install -g pnpm` to manage packages. Fast! https://pnpm.io/

- `pnpm install`
- `pnpm dev`
  * will run `watch` and `browsersync` in parallel
